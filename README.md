This is an attempt to convert [this](https://ffmpeg.org/doxygen/trunk/transcoding_8c_source.html) example into Java using [JavaCPP-Presets](https://github.com/bytedeco/javacpp-presets/tree/master/ffmpeg), and at the same time learn how to use FFmpeg.

## Get the code
1. `git clone https://rishabh9@bitbucket.org/rishabh9/javacpp-ffmpeg.git`
2. `cd javacpp-ffmpeg`

## Setup Vagrant to prevent 'it works on my box' scenario.
1. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
2. Install [Vagrant](https://www.vagrantup.com/downloads.html)
3. `vagrant up`

## SSH into Vagrant and run the code
1. `vagrant ssh`
2. `cd src`
3. `./gradlew clean build run`

## Executing Valgrind for debugging
This creates a Fat JAR and then executes Valgrind. See the `valgrindJava` file for more details.
1. `./valgrindJava`