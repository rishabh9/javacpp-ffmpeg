#!/usr/bin/env bash

#rm -rf /var/lib/apt/lists && \
apt-get update
apt-get -y install --no-install-recommends \
	git curl wget openjdk-8-jdk valgrind gdb zip unzip \
	build-essential pkg-config python automake make cmake \
	nasm yasm libxcb1-dev maven && \
echo "export JAVA_HOME=\"/usr/lib/jvm/java-8-openjdk-amd64\"" >> .bash_profile && \
echo "source ~/.bashrc" >> .bash_profile && \
source .bash_profile && \
chown ubuntu:ubuntu .bash_profile && \
ln -s /vagrant myproject && \
chown -R ubuntu:ubuntu src && \
git clone https://github.com/bytedeco/javacpp.git --branch master && \
git clone https://github.com/bytedeco/javacpp-presets.git --branch master && \
cd javacpp/ && \
mvn clean install deploy && \
cd ../javacpp-presets/ && \
./cppbuild.sh -platform linux-x86_64 clean ffmpeg && \
./cppbuild.sh -platform linux-x86_64 install ffmpeg && \
mvn --projects .,ffmpeg clean install -Djavacpp.platform=linux-x86_64 && \
cd platform/ && \
mvn --projects ../ffmpeg/platform install -Djavacpp.platform.host && \
cd ~/myproject &&
./gradlew clean build fatJar