package tutorial;

import org.bytedeco.javacpp.*;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.javacpp.avcodec.*;
import static org.bytedeco.javacpp.avfilter.*;
import static org.bytedeco.javacpp.avformat.*;
import static org.bytedeco.javacpp.avutil.*;

public class App {

    private App() {
    }

    private avformat.AVFormatContext inputAVFormatContext = new avformat.AVFormatContext(null);
    private avformat.AVFormatContext outputAVFormatContext = new avformat.AVFormatContext(null);
    private List<StreamContext> streamContextList = new ArrayList<>();
    private List<FilteringContext> filteringContextList = new ArrayList<>();

    // Setup certain variables
    private final avcodec.AVPacket packet = new avcodec.AVPacket();
    private final avutil.AVFrame frame = new avutil.AVFrame();

    public static void main(String[] args) {
        // TODO Remove later and accept file names from command line.
        String inputFile = "18404292.AVI";
        String outputFile = "18404292-2.AVI";
        final App app = new App();
        // Register all formats and codecs. To selectively enable formats,
        // use av_register_input_format() and av_register_output_format()
        av_register_all();
        avfilter_register_all();
        int result = app.openInputFile(inputFile);
        if (result != 0) {
            app.freeMemoryAndExit(result);
        }
        result = app.openOutputFile(outputFile);
        if (result != 0) {
            app.freeMemoryAndExit(result);
        }
        result = app.initializeFilters();
        if (result != 0) {
            app.freeMemoryAndExit(result);
        }
        // read all packets
        while (true) {
            result = av_read_frame(app.inputAVFormatContext, app.packet);
            if (result < 0) {
                break;
            }
            int streamIndex = app.packet.stream_index();
            int avMediaType = app.inputAVFormatContext.streams(streamIndex).codecpar().codec_type();
            av_log(null, AV_LOG_DEBUG, String.format("Demuxer gave frame of stream_index %d\n", streamIndex));
            if (null != app.filteringContextList.get(streamIndex).getFilterGraph()
                    && !app.filteringContextList.get(streamIndex).getFilterGraph().isNull()) {
                av_log(null, AV_LOG_DEBUG, "Going to reencode & filter the frame\n");
                AVFrame frame = av_frame_alloc();
                if (null == frame || frame.isNull()) {
                    result = AVERROR_ENOMEM();
                    break;
                }
                av_packet_rescale_ts(app.packet, app.inputAVFormatContext.streams(streamIndex).time_base(),
                        app.streamContextList.get(streamIndex).getDecodingContext().time_base());
                IntPointer gotFrame = new IntPointer();
                if (avMediaType == AVMEDIA_TYPE_VIDEO) {
                    result = avcodec_decode_video2(
                            app.streamContextList.get(streamIndex).getDecodingContext(), frame, gotFrame, app.packet);
                } else {
                    result = avcodec_decode_audio4(
                            app.streamContextList.get(streamIndex).getDecodingContext(), frame, gotFrame, app.packet);
                }
                if (result < 0) {
                    av_frame_free(frame);
                    av_log(null, AV_LOG_ERROR, "Decoding failed\n");
                    break;
                }
                if (!gotFrame.isNull()) {
                    frame.pts(frame.best_effort_timestamp());
                    result = app.filterEncodeWriteFrame(frame, streamIndex);
                    av_frame_free(frame);
                    if (result < 0) {
                        app.freeMemoryAndExit(result);
                    }
                } else {
                    av_frame_free(frame);
                }
            } else {
                // remux this frame without reencoding
                av_packet_rescale_ts(app.packet,
                        app.inputAVFormatContext.streams(streamIndex).time_base(),
                        app.outputAVFormatContext.streams(streamIndex).time_base());
                result = av_interleaved_write_frame(app.outputAVFormatContext, app.packet);
                if (result < 0) {
                    app.freeMemoryAndExit(result);
                }
            }
            av_packet_unref(app.packet);
        }
        // flush filters and encoders
        for (int i = 0; i < app.inputAVFormatContext.nb_streams(); i++) {
            // flush filter
            if (null != app.filteringContextList.get(i).getFilterGraph()
                    && !app.filteringContextList.get(i).getFilterGraph().isNull()) {
                continue;
            }
            result = app.filterEncodeWriteFrame(null, i);
            if (result < 0) {
                av_log(null, AV_LOG_ERROR, "Flushing filter failed\n");
                app.freeMemoryAndExit(result);
            }
            // flush encoder
            result = app.flushEncoder(i);
            if (result < 0) {
                av_log(null, AV_LOG_ERROR, "Flushing encoder failed\n");
                app.freeMemoryAndExit(result);
            }
        }
        av_write_trailer(app.outputAVFormatContext);
        // Lastly, exit normally after freeing memory
        app.freeMemoryAndExit(result);
    }

    private int flushEncoder(int streamIndex) {
        IntPointer gotFrame = new IntPointer();
        int flag = this.streamContextList.get(streamIndex).getEncodingContext().codec().capabilities()
                & AV_CODEC_CAP_DELAY;
        if (flag == 0) {
            return 0;
        }
        int result;
        while (true) {
            av_log(null, AV_LOG_INFO, String.format("Flushing stream #%d encoder\n", streamIndex));
            result = encodeWriteFrame(null, streamIndex, gotFrame);
            if (result < 0) {
                break;
            }
            if (gotFrame.isNull()) {
                return 0;
            }
        }
        return result;
    }

    private int filterEncodeWriteFrame(AVFrame frame, int streamIndex) {
        int result;
        AVFrame filterFrame;
        av_log(null, AV_LOG_INFO, "Pushing decoded frame to filters\n");
        // push the decoded frame into the filtergraph
        result = av_buffersrc_add_frame_flags(
                this.filteringContextList.get(streamIndex).getBufferSourceContext(), frame, 0);
        if (result < 0) {
            av_log(null, AV_LOG_ERROR, "Error while feeding the filtergraph\n");
            return result;
        }
        // pull filtered frames from the filtergraph
        while (true) {
            filterFrame = av_frame_alloc();
            if (null == filterFrame || filterFrame.isNull()) {
                result = AVERROR_ENOMEM();
                break;
            }
            av_log(null, AV_LOG_INFO, "Pulling filtered frame from filters\n");
            result = av_buffersink_get_frame(
                    this.filteringContextList.get(streamIndex).getBufferSinkContext(), filterFrame);
            if (result < 0) {
                /* if no more frames for output - returns AVERROR(EAGAIN)
                 * if flushed and no more frames for output - returns AVERROR_EOF
                 * rewrite retcode to 0 to show it as normal procedure completion
                 */
                if (result == AVERROR_EAGAIN() || result == AVERROR_EOF()) {
                    result = 0;
                }
                av_frame_free(filterFrame);
                break;
            }
            filterFrame.pict_type(AV_PICTURE_TYPE_NONE);
            result = encodeWriteFrame(filterFrame, streamIndex, null);
            if (result < 0) {
                break;
            }
        }
        return result;
    }

    private int encodeWriteFrame(AVFrame filterFrame, int streamIndex, IntPointer gotFrame) {
        int result;
        IntPointer gotFrameLocal = new IntPointer();
        AVPacket encodingPacket = new AVPacket();
        if (null == gotFrame || gotFrame.isNull()) {
            gotFrame = gotFrameLocal;
        }
        av_log(null, AV_LOG_INFO, "Encoding frame\n");
        // Encode filtered frame
        encodingPacket.data(null);
        encodingPacket.size(0);
        av_init_packet(encodingPacket);
        if (this.inputAVFormatContext.streams(streamIndex).codecpar().codec_type() == AVMEDIA_TYPE_VIDEO) {
            result = avcodec_encode_video2(this.streamContextList.get(streamIndex).getEncodingContext(), encodingPacket,
                    filterFrame, gotFrame);
        } else {
            result = avcodec_encode_audio2(this.streamContextList.get(streamIndex).getEncodingContext(), encodingPacket,
                    filterFrame, gotFrame);
        }
        av_frame_free(filterFrame);
        System.out.println("### 1");
        if (result < 0) {
            System.out.println("### 2");
            return result;
        }
        if (gotFrame.isNull()) {
            System.out.println("### 3");
            av_log(null, AV_LOG_WARNING, "gotframe is NULL\n");
            return 0;
        }
        System.out.println("### 4");
        // Prepare packet for muxing
        encodingPacket.stream_index(streamIndex);
        av_packet_rescale_ts(encodingPacket, this.streamContextList.get(streamIndex).getEncodingContext().time_base(),
                this.outputAVFormatContext.streams(streamIndex).time_base());
        av_log(null, AV_LOG_DEBUG, "Muxing frame\n");
        // mux encoded frame
        result = av_interleaved_write_frame(this.outputAVFormatContext, encodingPacket);
        return result;
    }

    private int initializeFilters() {
        for (int i = 0; i < inputAVFormatContext.nb_streams(); i++) {
            FilteringContext filteringContext = new FilteringContext();
            filteringContext.setBufferSourceContext(null);
            filteringContext.setBufferSinkContext(null);
            filteringContext.setFilterGraph(null);
            filteringContextList.add(i, filteringContext);
            int codecType = inputAVFormatContext.streams(i).codecpar().codec_type();
            if ((codecType == AVMEDIA_TYPE_AUDIO || codecType == AVMEDIA_TYPE_VIDEO)) {
                String filterSpec;
                if (codecType == AVMEDIA_TYPE_VIDEO) {
                    filterSpec = "null"; // passthrough (dummy) filter for video
                } else {
                    filterSpec = "anull"; // passthrough (dummy) filter for audio
                }
                int result = initializeFilter(filteringContext, streamContextList.get(i).getDecodingContext(),
                        streamContextList.get(i).getEncodingContext(), filterSpec);
                if (result != 0) {
                    return result;
                }
            }
        }
        return 0;
    }

    private int initializeFilter(FilteringContext filteringContext,
                                 AVCodecContext decodingContext, AVCodecContext encodingContext, String filterSpec) {
        avfilter.AVFilterInOut outputs = avfilter_inout_alloc();
        avfilter.AVFilterInOut inputs = avfilter_inout_alloc();
        avfilter.AVFilterGraph filterGraph = avfilter_graph_alloc();
        int result;
        if (null == outputs || outputs.isNull()
                || null == inputs || inputs.isNull()
                || null == filterGraph || filterGraph.isNull()) {
            return freeFiltersAndReturn(outputs, inputs, AVERROR_ENOMEM());
        }
        AVFilter bufferSource;
        AVFilter bufferSink;
        AVFilterContext bufferSourceContext = new AVFilterContext(null);
        AVFilterContext bufferSinkContext = new AVFilterContext(null);
        if (decodingContext.codec_type() == AVMEDIA_TYPE_VIDEO) {
            bufferSource = avfilter_get_by_name("buffer");
            bufferSink = avfilter_get_by_name("buffersink");
            if (null == bufferSource || bufferSource.isNull() || null == bufferSink || bufferSink.isNull()) {
                av_log(null, AV_LOG_ERROR, "filtering source or sink element not found\n");
                return freeFiltersAndReturn(outputs, inputs, AVERROR_UNKNOWN());
            }
            String formattedString = String.format("video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
                    decodingContext.width(), decodingContext.height(), decodingContext.pix_fmt(),
                    decodingContext.time_base().num(), decodingContext.time_base().den(),
                    decodingContext.sample_aspect_ratio().num(), decodingContext.sample_aspect_ratio().den());
            av_log(null, AV_LOG_INFO, formattedString + "\n");
            result = avfilter_graph_create_filter(
                    bufferSourceContext, bufferSource, "in", formattedString, null, filterGraph);
            if (result < 0) {
                av_log(null, AV_LOG_ERROR, "Cannot create buffer source\n");
                return freeFiltersAndReturn(outputs, inputs, result);
            }
            result = avfilter_graph_create_filter(
                    bufferSinkContext, bufferSink, "out", null, null, filterGraph);
            if (result < 0) {
                av_log(null, AV_LOG_ERROR, "Cannot create buffer sink\n");
                return freeFiltersAndReturn(outputs, inputs, result);
            }
            result = av_opt_set_bin(bufferSinkContext, "pix_fmts",
                    ByteBuffer.allocate(4).putInt(encodingContext.pix_fmt()), 4, AV_OPT_SEARCH_CHILDREN);
            if (result < 0) {
                av_log(null, AV_LOG_ERROR, "Cannot set output pixel format\n");
                return freeFiltersAndReturn(outputs, inputs, result);
            }
        } else if (decodingContext.codec_type() == AVMEDIA_TYPE_AUDIO) {
            bufferSource = avfilter_get_by_name("abuffer");
            bufferSink = avfilter_get_by_name("abuffersink");
            if (null == bufferSource || bufferSource.isNull() || null == bufferSink || bufferSink.isNull()) {
                av_log(null, AV_LOG_ERROR, "filtering source or sink element not found\n");
                return freeFiltersAndReturn(outputs, inputs, AVERROR_UNKNOWN());
            }
            if (0 == decodingContext.channel_layout()) {
                decodingContext.channel_layout(av_get_default_channel_layout(decodingContext.channels()));
            }
            String formattedString =
                    String.format("time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%X",
                            decodingContext.time_base().num(), decodingContext.time_base().den(),
                            decodingContext.sample_rate(),
                            av_get_sample_fmt_name(decodingContext.sample_fmt()).getString(),
                            decodingContext.channel_layout());
            av_log(null, AV_LOG_INFO, formattedString + "\n");
            result = avfilter_graph_create_filter(
                    bufferSourceContext, bufferSource, "in", formattedString, null, filterGraph);
            if (result < 0) {
                av_log(null, AV_LOG_ERROR, "Cannot create audio buffer source\n");
                return freeFiltersAndReturn(outputs, inputs, result);
            }
            result = avfilter_graph_create_filter(
                    bufferSinkContext, bufferSink, "out", null, null, filterGraph);
            if (result < 0) {
                av_log(null, AV_LOG_ERROR, "Cannot create audio buffer sink\n");
                return freeFiltersAndReturn(outputs, inputs, result);
            }
            result = av_opt_set_bin(bufferSinkContext, "sample_fmts",
                    ByteBuffer.allocate(4).putInt(encodingContext.sample_fmt()), 4, AV_OPT_SEARCH_CHILDREN);
            if (result < 0) {
                av_log(null, AV_LOG_ERROR, "Cannot set output sample format\n");
                return freeFiltersAndReturn(outputs, inputs, result);
            }
            result = av_opt_set_bin(bufferSinkContext, "channel_layouts",
                    ByteBuffer.allocate(8).putLong(encodingContext.channel_layout()), 8, AV_OPT_SEARCH_CHILDREN);
            if (result < 0) {
                av_log(null, AV_LOG_ERROR, "Cannot set output channel layout\n");
                return freeFiltersAndReturn(outputs, inputs, result);
            }
            result = av_opt_set_bin(bufferSinkContext, "sample_rates",
                    ByteBuffer.allocate(4).putInt(encodingContext.sample_rate()), 4, AV_OPT_SEARCH_CHILDREN);
            if (result < 0) {
                av_log(null, AV_LOG_ERROR, "Cannot set output sample rate\n");
                return freeFiltersAndReturn(outputs, inputs, result);
            }
        } else {
            av_log(null, AV_LOG_ERROR, "Unknown codec/media type.\n");
            return freeFiltersAndReturn(outputs, inputs, AVERROR_UNKNOWN());
        }
        // Endpoints for the filter graph.
        outputs.name(new BytePointer("in"));
        outputs.filter_ctx(bufferSourceContext);
        outputs.pad_idx(0);
        outputs.next(null);
        inputs.name(new BytePointer("out"));
        inputs.filter_ctx(bufferSinkContext);
        inputs.pad_idx(0);
        inputs.next(null);
        if (null == outputs.name() || outputs.name().isNull() || null == inputs.name() || inputs.name().isNull()) {
            return freeFiltersAndReturn(outputs, inputs, AVERROR_ENOMEM());
        }
        result = avfilter_graph_parse_ptr(filterGraph, filterSpec, inputs, outputs, null);
        if (result < 0) {
            return freeFiltersAndReturn(outputs, inputs, result);
        }
        result = avfilter_graph_config(filterGraph, null);
        if (result < 0) {
            return freeFiltersAndReturn(outputs, inputs, result);
        }
        // Fill FilteringContext
        filteringContext.setBufferSourceContext(bufferSourceContext);
        filteringContext.setBufferSinkContext(bufferSinkContext);
        filteringContext.setFilterGraph(filterGraph);
        return freeFiltersAndReturn(outputs, inputs, result);
    }

    private int freeFiltersAndReturn(AVFilterInOut outputs, AVFilterInOut inputs, int result) {
        avfilter_inout_free(outputs);
        avfilter_inout_free(inputs);
        return result;
    }

    private int openInputFile(String filename) {
        int result = avformat_open_input(inputAVFormatContext, filename, null, null);
        if (result < 0) {
            av_log(null, AV_LOG_ERROR, "Cannot open input file!\n");
            return result;
        }
        result = avformat_find_stream_info(inputAVFormatContext, (AVDictionary) null);
        if (result < 0) {
            av_log(null, AV_LOG_ERROR, "Cannot find stream information!\n");
            return result;
        }
        /*
         * The following C code does not require conversion, as 'stream_ctx' is only required to hold the references.
         * We maintain a list at Java end for the same.
         *
         * stream_ctx = av_mallocz_array(ifmt_ctx->nb_streams, sizeof(*stream_ctx));
         * if (!stream_ctx)
         *     return AVERROR(ENOMEM);
         */
        for (int i = 0; i < inputAVFormatContext.nb_streams(); i++) {
            AVStream stream = inputAVFormatContext.streams(i);
            avcodec.AVCodec decoder = avcodec_find_decoder(stream.codecpar().codec_id());
            if (null == decoder || decoder.isNull()) {
                av_log(null, AV_LOG_ERROR, String.format("Failed to find decoder for stream #%d\n", i));
                return AVERROR_DECODER_NOT_FOUND();
            }
            avcodec.AVCodecContext codecContext = avcodec_alloc_context3(decoder);
            if (null == codecContext || codecContext.isNull()) {
                av_log(null, AV_LOG_ERROR,
                        String.format("Failed to allocate the decoder context for stream #%d\n", i));
                return AVERROR_ENOMEM();
            }
            result = avcodec_parameters_to_context(codecContext, stream.codecpar());
            if (result < 0) {
                av_log(null, AV_LOG_ERROR,
                        String.format("Failed to copy decoder parameters to input decoder context for stream #%d\n", i));
                return result;
            }
            // Re-encode video & audio and remux subtitles etc.
            if (codecContext.codec_type() == AVMEDIA_TYPE_VIDEO || codecContext.codec_type() == AVMEDIA_TYPE_AUDIO) {
                if (codecContext.codec_type() == AVMEDIA_TYPE_VIDEO) {
                    codecContext.framerate(av_guess_frame_rate(inputAVFormatContext, stream, null));
                }
                // Open decoder
                result = avcodec_open2(codecContext, decoder, (AVDictionary) null);
                if (result < 0) {
                    av_log(null, AV_LOG_ERROR, String.format("Failed to open decoder for stream #%d\n", i));
                    return result;
                }
            }
            StreamContext streamContext = new StreamContext();
            streamContext.setDecodingContext(codecContext);
            // Set it at specific index
            streamContextList.add(i, streamContext);
        }
        av_dump_format(inputAVFormatContext, 0, filename, 0);
        return 0;
    }

    private int openOutputFile(String filename) {
        int result = avformat_alloc_output_context2(outputAVFormatContext, null, null, filename);
        if (result < 0) {
            av_log(null, AV_LOG_ERROR, "Error allocating output format context\n");
            return result;
        }
        if (null == outputAVFormatContext || outputAVFormatContext.isNull()) {
            av_log(null, AV_LOG_ERROR, "Could not create output context\n");
            return AVERROR_UNKNOWN();
        }
        for (int i = 0; i < inputAVFormatContext.nb_streams(); i++) {
            AVStream outputStream = avformat_new_stream(outputAVFormatContext, null);
            if (null == outputStream || outputStream.isNull()) {
                av_log(null, AV_LOG_ERROR, "Failed allocating output stream\n");
                return AVERROR_UNKNOWN();
            }
            AVCodecContext decodingContext = streamContextList.get(i).getDecodingContext();
            if (decodingContext.codec_type() == AVMEDIA_TYPE_VIDEO
                    || decodingContext.codec_type() == AVMEDIA_TYPE_AUDIO) {
                // In this example, we choose transcoding to same codec
                AVCodec encoder = avcodec_find_encoder(decodingContext.codec_id());
                if (null == encoder || encoder.isNull()) {
                    av_log(null, AV_LOG_FATAL, "Necessary encoder not found\n");
                    return AVERROR_INVALIDDATA();
                }
                AVCodecContext encodingContext = avcodec_alloc_context3(encoder);
                if (null == encodingContext || encodingContext.isNull()) {
                    av_log(null, AV_LOG_FATAL, "Failed to allocate the encoder context\n");
                    return AVERROR_ENOMEM();
                }
                /*
                 * In this example, we transcode to same properties (picture size, sample rate etc.).
                 * These properties can be changed for output streams easily using filters */
                if (decodingContext.codec_type() == AVMEDIA_TYPE_VIDEO) {
                    encodingContext.height(decodingContext.height());
                    encodingContext.width(decodingContext.width());
                    encodingContext.sample_aspect_ratio(decodingContext.sample_aspect_ratio());
                    // Take first format from list of supported formats
                    if (null == encoder.pix_fmts() || encoder.pix_fmts().isNull()) {
                        encodingContext.pix_fmt(decodingContext.pix_fmt());
                    } else {
                        encodingContext.pix_fmt(encoder.pix_fmts().get(0));
                    }
                    // Video time_base can be set to whatever is handy and supported by encoder
                    encodingContext.time_base(av_inv_q(decodingContext.framerate()));
                } else {
                    encodingContext.sample_rate(decodingContext.sample_rate());
                    encodingContext.channel_layout(decodingContext.channel_layout());
                    encodingContext.channels(av_get_channel_layout_nb_channels(encodingContext.channel_layout()));
                    // Take first format from list of supported formats
                    encodingContext.sample_fmt(encoder.sample_fmts().get(0));
                    encodingContext.time_base(av_make_q(1, encodingContext.sample_rate()));
                }
                // Third parameter can be used to pass settings to encoder
                result = avcodec_open2(encodingContext, encoder, (AVDictionary) null);
                if (result < 0) {
                    av_log(null, AV_LOG_ERROR, String.format("Cannot open video encoder for stream #%d\n", i));
                    return result;
                }
                result = avcodec_parameters_from_context(outputStream.codecpar(), encodingContext);
                if (result < 0) {
                    av_log(null, AV_LOG_ERROR,
                            String.format("Failed to copy encoder parameters to output stream #%d\n", i));
                    return result;
                }
                int flag = outputAVFormatContext.oformat().flags() & AVFMT_GLOBALHEADER;
                if (flag > 0) { // Global header required, set it.
                    encodingContext.flags(encodingContext.flags() | AV_CODEC_FLAG_GLOBAL_HEADER);
                }
                outputStream.time_base(encodingContext.time_base());
                streamContextList.get(i).setEncodingContext(encodingContext);
            } else if (decodingContext.codec_type() == AVMEDIA_TYPE_UNKNOWN) {
                av_log(null, AV_LOG_FATAL,
                        String.format("Elementary stream #%d is of unknown type, cannot proceed\n", i));
                return AVERROR_INVALIDDATA();
            } else {
                // If this stream must be remuxed
                AVStream inputStream = inputAVFormatContext.streams(i);
                result = avcodec_parameters_copy(outputStream.codecpar(), inputStream.codecpar());
                if (result < 0) {
                    av_log(null, AV_LOG_ERROR, String.format("Copying parameters for stream #%d failed\n", i));
                    return result;
                }
                outputStream.time_base(inputStream.time_base());
            }
        }
        av_dump_format(outputAVFormatContext, 0, filename, 1);
        // ### CODE REVIEW REQUIRED - BEGIN - I am not sure if this is the correct conversion of C code.
        int flag = outputAVFormatContext.oformat().flags() & AVFMT_NOFILE;
        if (flag == 0) {
            AVIOContext pb = new AVIOContext();
            result = avio_open(pb, filename, AVIO_FLAG_WRITE);
            outputAVFormatContext.pb(pb);
            if (result < 0) {
                av_log(null, AV_LOG_ERROR, String.format("Could not open output file '%s'", filename));
                return result;
            }
        }
        // ### CODE REVIEW REQUIRED - END - I am not sure if this is the correct conversion of C code.
        // init muxer, write output file header
        result = avformat_write_header(outputAVFormatContext, (AVDictionary) null);
        if (result < 0) {
            av_log(null, AV_LOG_ERROR, "Error occurred when opening output file\n");
            return result;
        }
        return 0;
    }

    private void freeMemoryAndExit(int result) {
        av_packet_unref(packet);
        av_frame_free(frame);
        for (int i = 0; i < this.inputAVFormatContext.nb_streams(); i++) {
            avcodec_free_context(this.streamContextList.get(i).getDecodingContext());
            if (null != this.outputAVFormatContext
                    && !this.outputAVFormatContext.isNull()
                    && this.outputAVFormatContext.nb_streams() > i
                    && null != this.outputAVFormatContext.streams(i)
                    && !this.outputAVFormatContext.streams(i).isNull()
                    && null != this.streamContextList.get(i).getEncodingContext()
                    && !this.streamContextList.get(i).getEncodingContext().isNull()) {
                avcodec_free_context(this.streamContextList.get(i).getEncodingContext());
            }
            if (null != this.filteringContextList
                    && !this.filteringContextList.isEmpty()
                    && null != this.filteringContextList.get(i).getFilterGraph()
                    && !this.filteringContextList.get(i).getFilterGraph().isNull()) {
                avfilter_graph_free(this.filteringContextList.get(i).getFilterGraph());
            }
        }
        if (null != this.filteringContextList && !this.filteringContextList.isEmpty()) {
            for (FilteringContext filteringContext : this.filteringContextList) {
                av_free(filteringContext);
            }
        }
        if (null != this.streamContextList && !this.streamContextList.isEmpty()) {
            for (StreamContext streamContext : this.streamContextList) {
                av_free(streamContext);
            }
        }
        avformat_close_input(this.inputAVFormatContext);
        if (null != this.outputAVFormatContext && !this.outputAVFormatContext.isNull()
                && 0 == (this.outputAVFormatContext.oformat().flags() & AVFMT_NOFILE)) {
            avio_closep(this.outputAVFormatContext.pb());
        }
        avformat_free_context(this.outputAVFormatContext);
        if (result < 0) {
            av_log(null, AV_LOG_ERROR, String.format("Error occurred: %d\n", result));
        }
        System.exit(result);
    }

}

class StreamContext extends Pointer {
    private avcodec.AVCodecContext decodingContext;
    private avcodec.AVCodecContext encodingContext;

    avcodec.AVCodecContext getDecodingContext() {
        return decodingContext;
    }

    void setDecodingContext(avcodec.AVCodecContext decodingContext) {
        this.decodingContext = decodingContext;
    }

    avcodec.AVCodecContext getEncodingContext() {
        return encodingContext;
    }

    void setEncodingContext(avcodec.AVCodecContext encodingContext) {
        this.encodingContext = encodingContext;
    }
}

class FilteringContext extends Pointer {
    private avfilter.AVFilterContext bufferSinkContext;
    private avfilter.AVFilterContext bufferSourceContext;
    private avfilter.AVFilterGraph filterGraph;

    avfilter.AVFilterContext getBufferSinkContext() {
        return bufferSinkContext;
    }

    void setBufferSinkContext(avfilter.AVFilterContext bufferSinkContext) {
        this.bufferSinkContext = bufferSinkContext;
    }

    avfilter.AVFilterContext getBufferSourceContext() {
        return bufferSourceContext;
    }

    void setBufferSourceContext(avfilter.AVFilterContext bufferSourceContext) {
        this.bufferSourceContext = bufferSourceContext;
    }

    avfilter.AVFilterGraph getFilterGraph() {
        return filterGraph;
    }

    void setFilterGraph(avfilter.AVFilterGraph filterGraph) {
        this.filterGraph = filterGraph;
    }
}